'use strict';
const GITLAB_URL = 'https://gitlab.com';

async function fetchProjects(
    {groupId = "", options = {}}
) {
    let projects = [];
    await fetch(`${GITLAB_URL}/api/v4/groups/${groupId}/projects?include_subgroups=true`, options)
        .then(response => response.json())
        .then(repositories => {
            repositories.forEach(repository => {
                projects.push({
                    id: repository['id'],
                    name: repository['path_with_namespace'],
                    default_branch: repository['default_branch']
                });
            });
            return projects;
        })
        .catch(error => console.error(error));
    return projects;
}

async function protectDefaultBranch(
    {projectId = 0, options = {}, branch = "", projectName = ""}
) {

    options['headers']["content-type"] = "application/json";
    options["body"] = JSON.stringify({
        "name": branch,
        "push_access_level": 0,
        "merge_access_level": 40,
    });

    options["method"] = "DELETE";
    const result = await fetch(`${GITLAB_URL}/api/v4/projects/${projectId}/protected_branches/${branch}`, options)
    if (result.status !== 204) {
        console.error(`Failed to unprotect default branch protection for project ${projectName}`);
        return
    } else {
        console.log(`Unprotected default branch protection for project ${projectName}`);
    }

    options["method"] = "POST";
    const protection_result = await fetch(`${GITLAB_URL}/api/v4/projects/${projectId}/protected_branches`, options)
    if (protection_result.status !== 201) {
        console.error(`Failed to protect default branch for project ${projectName}`);
        return
    }
    console.log(`Protected default branch for project ${projectName}`);
};

const GITLAB_TOKEN = process.env.GITLAB_TOKEN;
const GITLAB_GROUP_ID = process.env.GITLAB_GROUP_ID;

if ( typeof GITLAB_TOKEN === 'undefined' || typeof GITLAB_GROUP_ID === 'undefined' ) {
    console.error('GITLAB_TOKEN and GITLAB_GROUP_ID must be set');
    process.exit(1);
}

fetchProjects({groupId: GITLAB_GROUP_ID, options: {headers: {'Private-Token': GITLAB_TOKEN}}}).then(projects => {
    projects.forEach(project => {
        protectDefaultBranch({
            projectId: project.id,
            options: {headers: {'Private-Token': GITLAB_TOKEN}},
            branch: project.default_branch,
            projectName: project.name
        }).then(r => "ok");
    });
});
